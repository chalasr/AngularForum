-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Ven 02 Janvier 2015 à 17:54
-- Version du serveur: 5.5.40-0ubuntu0.14.04.1
-- Version de PHP: 5.5.9-1ubuntu4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `big_project`
--

-- --------------------------------------------------------

--
-- Structure de la table `answers`
--

CREATE TABLE IF NOT EXISTS `answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subcategory_id` int(11) NOT NULL,
  `subcategory_name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `author_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `author_name` varchar(255) NOT NULL,
  `thread_id` int(11) NOT NULL,
  `votes` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `answers`
--

INSERT INTO `answers` (`id`, `subcategory_id`, `subcategory_name`, `description`, `author_id`, `name`, `created_at`, `updated_at`, `author_name`, `thread_id`, `votes`) VALUES
(1, 2, '', 'yooo', 1, '', '2014-12-20 16:23:50', '2014-12-20 15:37:23', 'root', 1, 2),
(2, 2, '', 'Solution : \n$(document).ready(function () {\n		$(''#sortable'').sortable({\n			cursor: ''move'',\n			axis: ''y'',\n			update: function (event, ui) {\n				var order = $(this).sortable(''toArray'');\n				$.ajax({\n					data : {order : order},\n					type: ''POST'',\n					url: ''http://www.lateliermac.com/reorder''\n				});\n			}\n		});\n	});', 1, '', '2014-12-20 16:27:43', '2014-12-20 15:37:24', 'root', 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `categories`
--

INSERT INTO `categories` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'RESSOURCES', 'Vous chercher un tutoriel ou un exemple, c''est par ici !', '2014-12-20 00:00:00', '0000-00-00 00:00:00'),
(2, 'QUESTIONS', 'Vous avez un problème avec votre code ou cherchez un renseignement, c''est la bonne catégorie.', '2014-12-20 00:00:00', '0000-00-00 00:00:00'),
(3, 'PARTAGE', 'Le coin PARTAGE', '2014-12-20 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `followeds`
--

CREATE TABLE IF NOT EXISTS `followeds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `thread_id` int(11) NOT NULL,
  `author_name` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Contenu de la table `roles`
--

INSERT INTO `roles` (`id`, `name`) VALUES
(1, 'utilisateur'),
(3, 'administrateur');

-- --------------------------------------------------------

--
-- Structure de la table `sub_categories`
--

CREATE TABLE IF NOT EXISTS `sub_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `sub_categories`
--

INSERT INTO `sub_categories` (`id`, `name`, `description`, `category_id`, `created_at`, `updated_at`) VALUES
(1, 'PHP', 'Questions sur du PHP', 2, '2014-12-20 00:00:00', '0000-00-00 00:00:00'),
(2, 'Javascript', 'Questions concernant le Javascript', 2, '2014-12-27 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `threads`
--

CREATE TABLE IF NOT EXISTS `threads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subcategory_id` int(11) NOT NULL,
  `subcategory_name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `author_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `author_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `threads`
--

INSERT INTO `threads` (`id`, `subcategory_id`, `subcategory_name`, `description`, `author_id`, `name`, `created_at`, `updated_at`, `author_name`) VALUES
(1, 2, 'Javascript', 'Voici mon code : \n.... exemple ....\n\nJe cherche ce qui coince dans ma requête AJAX, si quelqu''un est passé par là , merci d''avance de me donner un coup de main.\n\nRobin', 38, 'Un tableau sortable sauvé en BDD', '2014-12-20 16:08:52', '2014-12-20 15:09:28', ''),
(2, 2, 'Javascript', 'Voici mon code : \r\n.... exemple ....\r\n\r\nJe cherche ce qui coince dans ma requête AJAX, si quelqu''un est passé par là , merci d''avance de me donner un coup de main.\r\n\r\nRobin', 38, 'Aidez moi, ma requête ne fonctionne pas', '2014-12-20 16:08:52', '2014-12-20 15:09:28', ''),
(3, 2, 'Javascript', 'Voici mon code : \r\n.... exemple ....\r\n\r\nJe cherche ce qui coince dans ma requête AJAX, si quelqu''un est passé par là , merci d''avance de me donner un coup de main.\r\n\r\nRobin', 38, 'Besoins de conseils pour sécuriser mon code', '2014-12-20 16:08:52', '2014-12-20 15:09:28', ''),
(4, 2, 'Javascript', 'Bonjour à tous ! <br> A votre avis, Laravel ou Symfony ? Pour une application de taille moyenne', 38, 'Laravel4 ou Symfony2 ?', '2014-12-20 16:08:52', '2014-12-20 15:52:10', '');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `role_id` int(11) NOT NULL DEFAULT '1',
  `remember_token` int(11) NOT NULL,
  `confirmation_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `confirmed` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unique` (`username`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `created_at`, `updated_at`, `role_id`, `remember_token`, `confirmation_code`, `confirmed`) VALUES
(1, 'root', '$2y$10$/r/4DIZVun9p7uCFuwE.c..jP2dODB9bWjUqCXv2h5iHJlYO1svrK', 'root@rout.fr', '2014-09-17 05:31:38', '2014-12-20 15:53:25', 3, 0, '', 1);
-- --------------------------------------------------------

--
-- Structure de la table `votes`
--

CREATE TABLE IF NOT EXISTS `votes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vote` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `thread_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `votes`
--

INSERT INTO `votes` (`id`, `vote`, `user_id`, `thread_id`, `created_at`, `updated_at`) VALUES
(1, 1, 38, 1, '2014-12-20 16:09:43', '2014-12-20 15:09:43'),
(2, 1, 1, 1, '2014-12-20 16:10:31', '2014-12-20 15:10:31'),
(3, -1, 1, 1, '2014-12-20 16:47:18', '2014-12-20 15:47:18'),
(4, 1, 1, 1, '2014-12-20 16:47:18', '2014-12-20 15:47:18');

-- --------------------------------------------------------

--
-- Structure de la table `votes_ans`
--

CREATE TABLE IF NOT EXISTS `votes_ans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vote` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `answer_id` int(11) NOT NULL,
  `thread_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `votes_ans`
--

INSERT INTO `votes_ans` (`id`, `vote`, `user_id`, `answer_id`, `thread_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 1, '2014-12-20 16:37:21', '2014-12-20 15:37:21'),
(2, 1, 1, 1, 1, '2014-12-20 16:37:23', '2014-12-20 15:37:23'),
(3, 1, 1, 2, 1, '2014-12-20 16:37:24', '2014-12-20 15:37:24');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
