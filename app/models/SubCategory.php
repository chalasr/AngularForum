<?php

class SubCategory extends Eloquent {

   protected $fillable = array('name', 'description', 'category_id');

    // public static $rules = array(
    //     'content' => 'required',
    // );

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sub_categories';

}