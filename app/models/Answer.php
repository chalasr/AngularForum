<?php

class Answer extends Eloquent {

    protected $fillable = array(
        'subcategory_id', 'thread_id', 'name', 'description', 'author_id' , 'author_name', 'votes'
    );

    // public static $rules = array(
    //     'content' => 'required',
    // );

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'answers';

}