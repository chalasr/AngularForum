<?php

class Vote extends Eloquent {

    protected $fillable = array(
        'thread_id', 'user_id', 'vote'
    );

    // public static $rules = array(
    //     'content' => 'required',
    // );

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'votes';

}