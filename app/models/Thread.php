<?php

class Thread extends Eloquent {

    protected $fillable = array(
        'subcategory_id', 'name', 'description', 'author_id' , 'author_name', 'subcategory_name'
    );

    // public static $rules = array(
    //     'content' => 'required',
    // );

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'threads';

    public static function show($datetime,$heure=false){
        $tmstamp=strtotime($datetime);
        $jour = array('Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche');
        $mois = array('Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre');
        $date=$jour[date("N",$tmstamp)-1]." ".date("d",$tmstamp)." ".$mois[date("n",$tmstamp)-1]." ".date("Y",$tmstamp);
        if($heure==true){
            $date.=" à".date("H:i:s",$tmstamp);
        }
        return ($date);
    }

}