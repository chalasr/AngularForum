<?php

class Role extends Eloquent {

   protected $fillable = array('name');

    // public static $rules = array(
    //     'content' => 'required',
    // );

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'roles';

}