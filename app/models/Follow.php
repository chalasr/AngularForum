<?php

class Follow extends Eloquent {

    protected $fillable = array(
        'thread_id', 'user_id', 'author_name'
    );

    // public static $rules = array(
    //     'content' => 'required',
    // );

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'followeds';

}