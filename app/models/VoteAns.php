<?php

class VoteAns extends Eloquent {

    protected $fillable = array(
        'answer_id', 'user_id', 'vote', 'thread_id'
    );

    // public static $rules = array(
    //     'content' => 'required',
    // );

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'votes_ans';

}