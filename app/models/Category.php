<?php

class Category extends Eloquent {

    protected $fillable = array('name', 'description');

    // public static $rules = array(
    //     'content' => 'required',
    // );

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'categories';

}