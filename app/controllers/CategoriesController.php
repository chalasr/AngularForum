<?php

class CategoriesController extends \BaseController {

	/**
	 * Send back all comments as JSON
	 *
	 * @return Response
	 */
	public function index()
	{
		return Response::json(Category::all());
	}

/**
*Lister les fichiers par utilisateur
*/
	public function myads()
	{
		$userid = "";
		if(Auth::check())
			$userid = Auth::user()->id;
		return Response::json(Category::where('user_id', '=', $userid)->get());
	}

	public function getSub($id){
		return Response::json(SubCategory::where('category_id','=',$id)->get());
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		if(Auth::user()->role_id == 3){
			Category::create(array(
				'name' => Input::get('author'),
				'description' => Input::get('text')
			));
			return Response::json(array('success' => true));
		}else{
			return Response::json(array('success' => false));
		}
	}


	/**
	 * Return the specified resource using JSON
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return Response::json(Category::find($id));
	}

/*
*UPDATE
*/
	public function update($id)
	{
		$comment = Category::findOrFail($id);
		if(Auth::user()->id == $comment->user_id){
			$datas['titre'] = Input::get('author');
			$datas['description'] = Input::get('text');
			$comment->update($datas);
			return Response::json(array('success' => true));
		}else{
			return Response::json(array('error' => true));
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Category::destroy($id);
		return Response::json(array('success' => true));
	}

}