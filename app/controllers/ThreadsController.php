<?php

class ThreadsController extends \BaseController {

	public function __construct(){
        //$this->beforeFilter('csrf', array('on' => 'post'));
        $this->beforeFilter('auth', array('only' => 'store'));
        $this->beforeFilter('auth', array('only' => 'update'));
    }

	/**
	 * Display list
	 *
	 * @return Response
	 */
	public function index(){
		//return Response::json(Thread::where('subcategory_id','=',$id)->get());
	}

	/**
	 * Create
	 *
	 * @return Response
	 */
	public function getThreads($id){
		return Response::json(Thread::where('subcategory_id','=',$id)->get());
	}


	/**
	 * View
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getView($id){
		$annonce  = Thread::where('id', '=', $id)->get();
		// $user  = User::where('id', '=', $annonce->id)->get();
        return View::make('annonces.show', compact("annonce"));
	}

	public function myThreads(){
		$userid = "";
		if(Auth::check())
			$userid = Auth::user()->id;
		return Response::json(Thread::where('author_id', '=', $userid)->get());
	}



	public function store(){
		$level = Thread::where('author_id', '=', Auth::user()->id)->count();
		$answer = Answer::where('author_id', '=', Auth::user()->id)->count();
		$subname = SubCategory::where('id', '=', Input::get('subcategory'))->get();
		foreach($subname as $sub){
			Thread::create(array(
				'name'           	=> Input::get('author'),
				'description'    	=> Input::get('text'),
				'subcategory_id' 	=> Input::get('subcategory'),
				'author_id' 	 	=> Auth::user()->id,
				'subcategory_name'  => $sub->name
			));
		}
		return Response::json(array('success' => true));
	}

	/**
	 * Return the specified resource using JSON
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id){
		return Response::json(Thread::find($id));
	}

/*
*UPDATE
*/
	public function update($id){
		$comment = Thread::findOrFail($id);
		if(Auth::user()->id == $comment->author_id){
			if(Input::get('author') != '')
				$datas['name'] = Input::get('author');
			if(Input::get('text') != '')
			$datas['description'] = Input::get('text');
			$comment->update($datas);

			$maillist = Follow::where('thread_id', '=', $id)->get();
			$thread = Thread::where('id','=', $id);
			foreach($maillist as $v) {
 				Mail::send('emails.newupdate', $v->toArray() , function($message) use($v) {
	                $message->from('robin.chalas@gmail.com', "W'HACKER|NET");
	                $message->to($v->author_name, 'Membre')
	                    ->subject("Modification d'un thread que vous suivez");
	            });
			}
			return Response::json(array('success' => true));
		}else{
			return Response::json(array('error' => true));
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id){
		Thread::destroy($id);
		return Response::json(array('success' => true));
	}

}
