<?php

class SubCategoriesController extends \BaseController {

	/**
	 * Send back all comments as JSON
	 *
	 * @return Response
	 */
	public function index()
	{
		return Response::json(SubCategory::all());
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		if(Auth::user()->role_id == 3){
			SubCategory::create(array(
				'category_id' => Input::get('category'),
				'name' => Input::get('author'),
				'description' => Input::get('text')
			));
			return Response::json(array('success' => true));
		}else{
			return Response::json(array('success' => false));
		}
	}


	/**
	 * Return the specified resource using JSON
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return Response::json(SubCategory::find($id));
	}

/*
*UPDATE
*/
	public function update($id)
	{
		$comment = SubCategory::findOrFail($id);
		if(Auth::user()->id == $comment->user_id){
			$datas['name'] = Input::get('author');
			$datas['description'] = Input::get('text');
			$comment->update($datas);
			return Response::json(array('success' => true));
		}else{
			return Response::json(array('error' => true));
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		SubCategory::destroy($id);
		return Response::json(array('success' => true));
	}

}