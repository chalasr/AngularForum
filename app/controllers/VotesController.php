<?php

class VotesController extends \BaseController {

	public function __construct(){
        //$this->beforeFilter('csrf', array('on' => 'post'));
        $this->beforeFilter('auth', array('only' => 'removeVote'));
        $this->beforeFilter('auth', array('only' => 'addVote'));
        static $canvote;
    }

	public function index(){

	}

	public function saveBvote(){

	}

	public function getVotes($id){
		return Response::json(Vote::where('thread_id','=',$id)->get()->sum('vote'));
	}


	public function haveVoted($id){
		return Response::json(Vote::where('user_id', '=', Auth::user()->id)
				->where('thread_id', '=', $id, 'AND')->get()->sum('vote'));
	}

	public function getVoteAns($id){
		return Response::json(VoteAns::where('answer_id', '=', $id)->get()->sum('vote'));
	}

	public function store(){
			Vote::create(array(
				'vote'    	        => Input::get('vote'),
				'thread_id'			=> Input::get('threadid'),
				'user_id' 	 	    => Auth::user()->id
			));
			return Response::json(array('success' => true));
	}

	public function storeAns(){
		$sumall = VoteAns::where('user_id','=', Auth::user()->id)
					->where('answer_id', '=', Input::get('answerid'), 'AND')->get()->sum('vote');
		if(Auth::check()){
			// if(Input::get('voteans') == 1){
			// 	if($sumall == 0){
					VoteAns::create(array(
						'vote'    	        => Input::get('voteans'),
						'answer_id'			=> Input::get('answerid'),
						'thread_id'			=> Input::get('threadid'),
						'user_id' 	 	    => Auth::user()->id
					));
					$answerid = Input::get('answerid');
					$comment = Answer::findOrFail($answerid);
					$votesum = VoteAns::where('answer_id', '=', $answerid)->get()->sum('vote');
					$newvote = VoteAns::orderBy('id', 'desc')->limit(1)->lists('vote');
					$datas['votes'] = $votesum;
					$comment->update($datas);
					return Response::json(array('success' => true));
				// }elseif($sumall > 0){
			 // 		return Response::json(array('status' => 400));
			 // 	}
			/*}elseif(Input::get('voteans') == -1){
				if($sumall == 0){
					VoteAns::create(array(
						'vote'    	        => Input::get('voteans'),
						'answer_id'			=> Input::get('answerid'),
						'thread_id'			=> Input::get('threadid'),
						'user_id' 	 	    => Auth::user()->id
					));
					$answerid = Input::get('answerid');
					$comment = Answer::findOrFail($answerid);
					$votesum = VoteAns::where('answer_id', '=', $answerid)->get()->sum('vote');
					$newvote = VoteAns::orderBy('id', 'desc')->limit(1)->lists('vote');
					$datas['votes'] = $votesum;
					$comment->update($datas);
					return Response::json(array('success' => true));
				}elseif($sumall < 0){
			 		return Response::json(array('status' => 400));
			 	}
			}*/
		}else{
			return Response::json(array('error' => true));
		}
	}


	public function sumAllAns($id, $answerid=NULL){
		if(Auth::check())
			return Response::json(VoteAns::where('user_id','=', Auth::user()->id)
					->where('thread_id', '=', $id, 'AND')
					->where('answer_id', '=', $answerid, 'AND')->get()->sum('vote'));
	}

	public function sumVotes($id){
		if(Auth::check())
			return Response::json(Vote::where('user_id','=', Auth::user()->id)
					->where('thread_id', '=', $id, 'AND')->get()->sum('vote'));
	}

	public function sumVoteAns($id){
		if(Auth::check())
			return Response::json(VoteAns::where('user_id','=', Auth::user()->id)
					->where('answer_id', '=', $id, 'AND')->get()->sum('vote'));
	}

	/**
	 * Return the specified resource using JSON
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id){
		return Response::json(Vote::find($id));
	}

/*
*UPDATE
*/
	public function updateVote($id){
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id){
		Vote::destroy($id);
		return Response::json(array('success' => true));
	}

}
