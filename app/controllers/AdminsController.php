<?php

class AdminsController extends \BaseController {

    public function __construct(){
        $this->beforeFilter('auth', array('only'=>array('index', 'getForum', 'getUsers', 'getCategories')));
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$users = DB::table('users')->orderBy('id', 'DESC')->take(10)->skip(0)->get();
	    $threads = DB::table('threads')->orderBy('id', 'DESC')->take(10)->skip(0)->get();
	    $answers = DB::table('answers')->orderBy('thread_id', 'DESC')->take(10)->skip(0)->get();
	    if(Auth::user()->role_id == 2 || Auth::user()->role_id == 3)
	        return View::make('admin.recap')->with('users', $users)->with('threads', $threads)->with('answers', $answers);
	    else
	        return Redirect::to('/')->with('message', 'Vous n\'avez pas accez à cette partie du site');
	}

	public function getForum(){
		$categories = Category::all();
		$subcats = SubCategory::all();
			return View::make('admin.forum')->with('categories', $categories)
											->with('subcats', $subcats);
	}


	public function getUsers(){
		if(Auth::user()->role_id == 3){
			$users = User::paginate(10);
			return View::make('admin.membres')->with('users', $users);
		}else{
			return Redirect::to('/')->with('error', 'Vous n\'avez pas accès à cette section !');
		}
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getThreads()
	{
		if(Auth::user()->role_id == 3){
			$threads = Thread::paginate(10);
			return View::make('admin.threads')->with('threads', $threads);
		}else{
			return Redirect::to('/')->with('error', 'Vous n\'avez pas accès à cette section !');
		}
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function getAnswers()
	{
		if(Auth::user()->role_id == 3){
			$answers = Answer::paginate(10);
			return View::make('admin.answers')->with('answers', $answers);
		}else{
			return Redirect::to('/')->with('error', 'Vous n\'avez pas accès à cette section !');
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
