<?php

class FollowsController extends \BaseController {

	public function __construct(){
        $this->beforeFilter('auth', array('only' => 'removeVote'));
        $this->beforeFilter('auth', array('only' => 'addVote'));
    }

	public function index(){

	}

	public function store(){
			Follow::create(array(
				'thread_id'			=> Input::get('threadid'),
				'author_name' 	 	=> Auth::user()->email,
				'user_id'			=> Auth::user()->id
			));
			return Response::json(array('success' => true));
	}

	public function getFollowers($id){
		if(Auth::check())
			return Response::json(Follow::where('user_id','=', Auth::user()->id)
					->where('thread_id', '=', $id, 'AND')->get()->count());
	}

	/**
	 * Return the specified resource using JSON
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id){
		return Response::json(Follow::find($id));
	}

	public function update($id){

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id){
		Follow::destroy($id);
		return Response::json(array('success' => true));
	}

}
