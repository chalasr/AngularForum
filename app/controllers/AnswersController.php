<?php

class AnswersController extends \BaseController {

	public function __construct(){
        //$this->beforeFilter('csrf', array('on' => 'post'));
        $this->beforeFilter('auth', array('only' => 'store'));
        $this->beforeFilter('auth', array('only' => 'update'));
    }

	/**
	 * Display list
	 *
	 * @return Response
	 */
	public function index(){
		//return Response::json(Answer::where('subcategory_id','=',$id)->get());
	}

	public function getfolloweds($id){
		return Response::json(Follow::where('thread_id', '=', $id)->lists('author_name'));
		//return Response::json($follow['author_name']);
	}
	/**
	 * Create
	 *
	 * @return Response
	 */
	public function getAnswers($id){
		return Response::json(Answer::where('thread_id','=',$id)->get());
	}

	/**
	 * View
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getView($id){
		$annonce  = Answer::where('id', '=', $id)->get();
		// $user  = User::where('id', '=', $annonce->id)->get();
        return View::make('annonces.show', compact("annonce"));
	}

	public function myThreads(){
		$userid = "";
		if(Auth::check())
			$userid = Auth::user()->id;
		return Response::json(Answer::where('author_id', '=', $userid)->get());
	}

	public function bestAnswers(){
		return Response::json(Answer::orderBy('votes', 'desc')->take(5)->skip(0)->get());
	}

	public function store(){
		if(Auth::check()){
			Answer::create(array(
				'description'    	=> Input::get('text'),
				'thread_id'			=> Input::get('threadid'),
				'subcategory_id'    => Input::get('subcategory'),
				'author_id' 	 	=> Auth::user()->id,
				'author_name'       => Auth::user()->username
			));
            $threadid = Input::get('threadid');
			$maillist = Follow::where('thread_id', '=', $threadid)->get();
			foreach($maillist as $v) {
 				Mail::send('emails.newanswer', array('key' => 'value') , function($message) use($v) {
	                $message->from('robin.chalas@gmail.com', 'Free-Ads');
	                $message->to($v->author_name, 'connerie')
	                    ->subject('Nouvelle réponse sur un thread que vous suivez');
	            });
			}
			return Response::json(array('success' => true));
		}else{
			return Response::json(array('error' => true));
		}
	}

	/**
	 * Return the specified resource using JSON
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id){
		return Response::json(Answer::find($id));
	}

/*
*UPDATE
*/
	public function update($id){
		$comment = Answer::findOrFail($id);
		if(Auth::user()->id == $comment->author_id){
			if(Input::get('author') != '')
				$datas['name'] = Input::get('author');
			if(Input::get('text') != '')
			$datas['description'] = Input::get('text');
			$comment->update($datas);
			return Response::json(array('success' => true));
		}else{
			return Response::json(array('error' => true));
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id){
		Answer::destroy($id);
		return Response::json(array('success' => true));
	}

}
