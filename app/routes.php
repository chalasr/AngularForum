<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
$alphanum = '[a-zA-Z0-9]+';
$id = '[0-9]+';
Route::get('/', function()
{
	//$subcats = DB::table('threads')->leftJoin('sub_categories', 'sub_categories.id', '=', 'threads.subcategory_id')->get();
	$subcats = SubCategory::all();
	return View::make('categories.index')->with('subcats', $subcats);
});

//Route::get('myads', array('uses' => 'CommentController@myads', 'as' => 'myads'));
Route::group(array('prefix' => 'api'), function() {
	$id = '[0-9]+';
	Route::get('getvotes/{id}', array('uses' => 'VotesController@getVotes', 'as' => 'getvotes'))->where('id', $id);
	Route::get('getvoteans/{id}', array('uses' => 'VotesController@getVoteAns'))->where('id', $id);
	Route::get('sumvotes/{id}', array('uses' => 'VotesController@sumVotes', 'as' => 'sumvotes'))->where('id', $id);
	Route::get('sumvoteans/{id}', array('uses' => 'VotesController@sumVoteAns', 'as' => 'sumvotes'))->where('id', $id);
	Route::get('sumallans/{id}', array('uses' => 'VotesController@sumAllAns'))->where('id', $id);
	Route::get('sumallans/{id}/{answerid}', array('uses' => 'VotesController@sumAllAns', 'as' => 'sumallvotes'))->where('id', $id);
	Route::get('mythreads', array('uses' => 'ThreadsController@myThreads', 'as'=> 'mythreads'));
	Route::get('getsubcategories/{id}', array('uses' => 'CategoriesController@getSub', 'as' => 'subcats'))->where('id', $id);
	Route::get('showthreads/{id}', array('uses' => 'ThreadsController@getThreads', 'as' => 'threads'))->where('id', $id);
	Route::get('getanswers/{id}', array('uses' => 'AnswersController@getAnswers', 'as' => 'answers'))->where('id', $id);
	Route::get('getfollows/{id}', array('uses' => 'FollowsController@getFollowers', 'as' => 'follows'))->where('id', $id);
	Route::get('getfolloweds/{id}', array('uses' => 'AnswersController@getfolloweds', 'as' => 'follows'))->where('id', $id);
	Route::get('bestanswers', array('uses' => 'AnswersController@bestAnswers', 'as' => 'tenbests'));
	Route::post('ansvotes', array('uses' => 'VotesController@storeAns'));
//Resources
	Route::resource('categories', 'CategoriesController',array('except' => array('create', 'edit')));
	Route::resource('threads', 'ThreadsController',array('except' => array('create', 'edit')));
	Route::resource('answers', 'AnswersController',array('except' => array('create', 'edit')));
	Route::resource('votes', 'VotesController',array('except' => array('create', 'edit')));
	Route::resource('follows', 'FollowsController',array('except' => array('create', 'edit')));
	Route::resource('subcategories', 'SubCategoriesController',array('except' => array('create', 'edit')));
});

Route::get('delete/annonce/{id}', 'ThreadsController@getDelete')->where('id', $id);
Route::get('{id}', 'ThreadsController@getView')->where('id', $id);
Route::resource('annonce', 'ThreadsController');
//Route::get('/', array('uses' => 'IndexController@showIndex'));
Route::post('search', array('uses' => 'IndexController@searchAds'));
Route::controller('users', 'UsersController');
Route::get('inscription', array('uses' => 'UsersController@getRegister', 'as' => 'inscription'));
Route::get('inscription/confirm/{confirmation_code}', array('uses' => 'UsersController@confirm', 'as' => 'confirm'))
																			->where('confirmation_code', $alphanum);
Route::get('login', array('uses' => 'UsersController@getLogin', 'as' => 'login'));

Route::group(['before' => 'auth'], function(){
    $id = '[0-9]+';
    Route::get('edit/{id}', array('uses' => 'UsersController@getEdit', 'as' => 'edit'))->where('id', $id);
    Route::post('edit/{id}', 'UsersController@postEdit')->where('id', $id);
    Route::get('delete/{id}', 'UsersController@getDelete')->where('id', $id);
    Route::get('account', array('uses' => 'UsersController@showAccount'));
    Route::get('admin/forum', ['uses' => 'AdminsController@getForum']);
    Route::get('admin/membres', ['uses' => 'AdminsController@getUsers']);
    Route::get('admin/threads', ['uses' => 'AdminsController@getThreads']);
    Route::get('admin/answers', ['uses' => 'AdminsController@getAnswers']);
});

//Route::controller('admin', 'AdminsController');
Route::resource('utilisateur', 'UsersController');
Route::resource('messages', 'MessagesController');
Route::resource('admin', 'AdminsController');