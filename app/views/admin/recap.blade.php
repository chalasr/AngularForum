@extends('layouts.admin')

@section('content')
<div class="row well"></div>
<div class="alert alert-success"></div>
<div class="alert alert-danger"></div>
<div class="articleindex body-admin">
    <div class="col-md-8 col-md-offset-2">
    <div class="panel panel-default">
        <hr />
        <div class="drop101">
            <span class="last10">10 derners inscrits &nbsp; <i class="crudadm fa fa-user"></i></span><a class="voir btn btn-default btn-sm" href="{{ URL::to('admin/membres') }}">Voir tous</a>
            <hr />
        </div>
        <div class="tab2">
            <table class="table">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Username</th>
                        <th>E-mail</th>
                        <th>Rôle </th>
                        <th>Actions</th>
                    </tr>
                </thead>

                <tbody>
                 @foreach($users as $user)
                    <tr class="tr">
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->username }}</td>
                        <td>{{ $user->email }}</td>
                        <td><?php if($user->role_id == 3) echo "Administrateur"; elseif($user->role_id == 2) echo "Moderateur"; else echo "Utilisateur"; ?></td>
                        <td>
                            <span class="crudadm fa fa-pencil"  onclick="location='{{ URL::to('edit/'.$user->id) }}';"></span> &nbsp;
                            <span class="crudadm fa fa-remove" onclick="location='{{ URL::to('delete/'.$user->id) }}';"></span>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="panel panel-default">
        <hr />
        <div class="drop102">
            <span class="last10">10 derniers threads &nbsp;<i class="crudadm fa fa-comment"></i></span><a class="voir btn btn-default btn-sm" href="{{ URL::to('admin/threads') }}">Voir tous</a>
            <hr />
        </div>
        <div class="tab1">
            <table class="table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nom</th>
                        <th>SubCategory</th>
                        <th>Publié le</th>
                        <th>Actions</th>
                    </tr>
                </thead>

                <tbody>
                 @foreach($threads as $v)
                    <tr>
                        <td>{{ $v->id }}</td>
                        <td>{{ $v->name }}</td>
                        <td>{{ $v->subcategory_name }}</td>
                        <td>{{ Thread::show($v->created_at) }}</td>
                        <td>
                            <span class="crudadm fa fa-eye" onclick="location='{{ URL::to($v->id) }}'"></span> &nbsp;
                            <span class="crudadm fa fa-remove" ng-click="deleteThread({{ $v->id }})"></span>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="panel panel-default">
        <hr />
        <div class="drop103">
            <span class="last10">10 dernieres réponses &nbsp;<i class="crudadm fa fa-reply"></i></span><a class="voir btn btn-default btn-sm" href="{{ URL::to('admin/answers') }}">Voir tous</a>
            <hr />
        </div>
        <div class="tab3">
            <table class="table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Thread id</th>
                        <th>Contenu </th>
                        <th>Author Name </th>
                        <th>Actions</th>
                    </tr>
                </thead>

                <tbody>
                 @foreach($answers as $file)
                    <tr>
                        <td>{{ $file->id }}</td>
                        <td>{{ $file->thread_id }}</td>
                        <td>{{ str_limit($file->description, 150) }}</td>
                        <td>{{ $file->author_name }}</td>
                        <td>
                            <span class="crudadm fa fa-pencil"></span> &nbsp;
                            <span class="crudadm fa fa-remove" ng-click="deleteAnswer({{ $file->id }})"></span>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    </div>
</div>
@stop
