@extends('layouts.admin')

@section('content')
    <div class="row well">
        <form id="formpost" ng-submit="submitCategory()">
            <div class="createform">
                <div class="form-group">
                </div>
                <div class="form-group">
                    <input class="titinput" type="text" class="form-control input-sm" name="author" ng-model="commentData.author" placeholder="Nom">
                </div>
                <div class="form-group">
                    <textarea class="form-control" id="textadesc" name="text" ng-model="commentData.text" cols="30" rows="5" placeholder="Description de la catégorie"></textarea>
                </div>
                <div class="form-group text-left subads">
                    <button type="submit" class="subbtn btn btn-primary ">Créer</button>
                </div>
                <pre>
                    <% commentData %>
                </pre>
            </div>
        </form>
        <form id="formsub" ng-submit="submitSubCat()">
            <div class="createform">
                <div class="form-group">
                    <select class="form-control input-sm titinput" name="category" ng-model="subData.category" placeholder="SubCatégory">
                    @foreach($categories as $subcat)
                        <option value="{{ $subcat->id }}">{{ $subcat->name }}</option>
                    @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <input class="titinput" type="text" class="form-control input-sm" name="author" ng-model="subData.author" placeholder="Nom">
                </div>
                <div class="form-group">
                    <textarea class="form-control" id="textadesc" name="text" ng-model="subData.text" cols="30" rows="5" placeholder="Description de la sous-catégorie"></textarea>
                </div>
                <div class="form-group text-left subads">
                    <button type="submit" class="subbtn btn btn-primary ">Créer</button>
                </div>
                <pre>
                    <% subData %>
                </pre>
            </div>
        </form>
    </div>

<div class="alert alert-success"></div>
<div class="alert alert-danger"></div>
<div class="articleindex body-admin">
    <div class="col-md-8 col-md-offset-2">
    <div class="panel panel-default">
        <hr />
        <div class="drop101">
            <span class="last10">CATEGORIES &nbsp; <i class="crudadm fa fa-user"></i></span><span id="newcategory" class="voir btn btn-default btn-sm">New Category</span>
            <hr />
        </div>
        <div class="tab2">
            <table class="table">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Actions</th>
                    </tr>
                </thead>

                <tbody>
                    <tr class="tr" ng-repeat="categorie in categories">
                        <td><% categorie.id %></td>
                        <td><% categorie.name %></td>
                        <td>
                            <span class="crudadm fa fa-pencil"></span> &nbsp;
                            <span class="crudadm fa fa-remove" ng-click="deleteCat(categorie.id)"></span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="panel panel-default">
        <hr />
        <div class="drop102">
            <span class="last10">SOUS CATEGORIES &nbsp;<i class="crudadm fa fa-folder-open"></i></span><span id="newsub" class="voir btn btn-default btn-sm">New Sub-Category</span>
            <hr />
        </div>
        <div class="tab1">
            <table class="table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Category ID</th>
                        <th>Nom</th>
                        <th>Actions</th>
                    </tr>
                </thead>

                <tbody>
                    <tr class="tr" ng-repeat="subcat in subcats">
                        <td><% subcat.id %></td>
                        <td><% subcat.category_id %></td>
                        <td><% subcat.name %></td>
                        <td>
                            <span class="crudadm fa fa-pencil"></span> &nbsp;
                            <span class="crudadm fa fa-remove" ng-click="deleteSubcat(subcat.id)"></span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    </div>
</div>
@stop