@extends('layouts.admin')

@section('content')
<div class="row well"></div>
<div class="alert alert-success"></div>
<div class="alert alert-danger"></div>
<div class="articleindex body-admin">
   <div class="panel panel-default">
        <hr />
        <div class="drop102">
            <span class="last10">THREADS &nbsp;<i class="crudadm fa fa-folder-open"></i></span><a class="voir btn btn-default btn-sm" href="{{ URL::to('admin/threads') }}">NEW</a>
            <hr />
        </div>
        <div>
            <table class="table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nom</th>
                        <th>SubCategory</th>
                        <th>Author Name </th>
                        <th>Actions</th>
                    </tr>
                </thead>

                <tbody>
                 @foreach($threads as $v)
                    <tr>
                        <td>{{ $v->id }}</td>
                        <td>{{ $v->name }}</td>
                        <td>{{ $v->subcategory_name }}</td>
                        <td>{{ $v->author_name }}</td>
                        <td>
                            <span class="crudadm fa fa-eye" onclick="location='{{ URL::to($v->id) }}'"></span> &nbsp;
                            <span class="crudadm fa fa-remove" ng-click="deleteThread({{ $v->id }})"></span>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="pagination">
    <ul class="pagination">
        <?php echo $threads->links(); ?>
    </ul>
    </div>
</div>
@stop