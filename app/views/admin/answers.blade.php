@extends('layouts.admin')

@section('content')
<div class="row well"></div>
<div class="alert alert-success"></div>
<div class="alert alert-danger"></div>
<div class="articleindex body-admin">
    <div class="panel panel-default">
        <hr />
        <div class="drop102">
            <span class="last10">RÉPONSES &nbsp;<i class="crudadm fa fa-reply"></i></span><a class="voir btn btn-default btn-sm" href="">NEW</a>
            <hr />
        </div>
        <div>
            <table class="table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Thread id</th>
                        <th>Contenu </th>
                        <th>Author Name </th>
                        <th>Actions</th>
                    </tr>
                </thead>

                <tbody>
                 @foreach($answers as $v)
                    <tr>
                        <td>{{ $v->id }}</td>
                        <td>{{ $v->thread_id }}</td>
                        <td>{{ str_limit($v->description, 150) }}</td>
                        <td>{{ $v->author_name }}</td>
                        <td>
                            <span class="crudadm fa fa-pencil"></span> &nbsp;
                            <span class="crudadm fa fa-remove" ng-click="deleteAnswer({{ $v->id }})"></span>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="pagination">
    <ul class="pagination">
        <?php echo $answers->links(); ?>
    </ul>
    </div>
</div>
@stop