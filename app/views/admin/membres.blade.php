@extends('layouts.admin')

@section('content')
<div class="row well"></div>
<div class="alert alert-success"></div>
<div class="alert alert-danger"></div>
<div class="articleindex body-admin">
    <div class="panel panel-default">
        <hr />
        <div class="drop101">
            <span class="last10">MEMBRES &nbsp; <i class="fa fa-user"></i></span><span id="newmember" class="voir btn btn-default btn-sm">NEW
            </span>
            <hr />
        </div>
        <table class="table">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Username</th>
                    <th>E-mail</th>
                    <th>Rôle </th>
                    <th>Actions</th>
                </tr>
            </thead>

            <tbody>
             @foreach($users as $user)
                <tr>
                   <td>{{ $user->id }}</td>
                        <td>{{ $user->username }}</td>
                        <td>{{ $user->email }}</td>
                        <td><?php if($user->role_id == 3) echo "Administrateur"; elseif($user->role_id == 2) echo "Moderateur"; else echo "Utilisateur"; ?>
                        </td>
                        <td>
                             <span class="crudadm fa fa-pencil"  onclick="location='{{ URL::to('edit/'.$user->id) }}';"></span> &nbsp;
                            <span class="crudadm fa fa-remove" onclick="location='{{ URL::to('delete/'.$user->id) }}';"></span>
                        </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <hr />
    </div>
    <div class="pagination">
    <ul class="pagination">
        <?php echo $users->links(); ?>
    </ul>
    </div>
</div>
@stop