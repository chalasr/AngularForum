<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="http://localhost/Big_project/forum/public/css/images/logo.ico" />
    <title>W'HACKER|NET</title>
    <!-- CSS -->
    {{ HTML::style('css/bootstrap.css') }}
    {{ HTML::style('css/flat2.css') }}
    {{ HTML::style('css/style.css') }}
    <!-- JS -->
    {{ HTML::script('js/jquery.js') }}
    {{ HTML::script('js/angular.min.js') }}
    {{ HTML::script('js/bootstrap.min.js') }}

    <!-- ANGULAR -->
    {{ HTML::script('js/controllers/mainCtrl.js') }}
    {{ HTML::script('js/services/commentService.js') }}
    {{ HTML::script('js/app.js') }}
</head>
<body ng-app="commentApp" ng-controller="mainController">
<!-- <img src="http://localhost/Big_project/forum/public/css/images/original(copie).png" id="original"/> -->
<header class="nav sub-menu @if(Auth::check()) menu @else menu2 @endif">
    <h4 class="logohead">
        <a id="logolink" href="{{ URL::to('/')}}">
            <span id="bigletter">W</span>'HACKER | NET <span class="sigle">α</span>
            <img src="http://localhost/Big_project/forum/public/css/images/logo.png" class="logo1" alt="logo"/>
        </a>
    </h4>
    <nav class="container">
        <ul class="menulist list-unstyled">

            @if(Auth::check())
                <li id="newpost"><i class="iconup fa fa-pencil-square-o"></i> <span>Nouveau thread</span></li>
                <li ng-click="showMyThreads()"><i class="iconup fa fa-folder-open"></i> <span>Mes threads</span></li>
                    <a id="dLabel" role="button" data-toggle="dropdown" data-target="#" href="#">
                        <div class="dropdown">
                            <li>
                            <span>
                                <?php $pseudo = Auth::user()->username; ?>
                                <i class="iconup fa fa-user"></i>&nbsp;
                                <?php echo $pseudo; ?>
                                <span class="caret"></span>
                            </span>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                                <a href="{{ URL::to('account') }}"><li>Mon compte</li></a>
                                <?php $url = action('UsersController@getLogout'); ?>
                                @if(Auth::user()->role_id == 3)
                                <a href="{{ URL::to('admin') }}"><li>ADMINISTRATION</li></a>
                                @endif
                                <a href="<?= $url; ?>"><li>Logout</li></a>
                            </ul>
                    </li>
                        </div>
                </a>
            @else
                <li> <i class="iconup fa fa-pencil"></i> <a href="{{ URL::to('inscription') }}">Inscription</a></li>
                <li> <i class="iconup fa fa-sign-in"></i> <a href="{{ URL::to('login') }}">Connexion</a></li>
            @endif
        </ul>
    </nav>
    @if(Auth::check())
        <?php $url = action('UsersController@getLogout'); ?>
        <?php $pseudo = Auth::user()->username; ?>
        <div style="display:none;">
           <p class="login-bouton info-user">
               <span class="btn btn-primary userinfo btn-sm"><?php echo $pseudo; ?> &nbsp;&nbsp;&nbsp;<i class="fa fa-user"></i></a>
           </p>
        </div>
    @endif
</header>

    <div id="content">
        @if(Session::has('message'))
            <p class="alert alert-success">{{ Session::get('message') }}</p>
        @elseif(Session::has('error'))
            <p class="alert alert-danger">{{ Session::get('error') }}</p>
        @endif
        @yield('content')
    </div>

    <footer>
        <p>My BigProject with &nbsp;[ Laravel - Angular.JS - WebSockets - WebRTC ] &nbsp; {chalas_r} </p>
    </footer>
     {{ HTML::script('js/script.js') }}

</body>
</html>