@extends('layouts.master')

@section('content')

<div ng-controller="mainController" class="col-md-8 col-md-offset-2">
	<div class="row well">
		<form id="formpost" ng-submit="submitComment()">
			<div class="createform">
				<div class="form-group">
					<input id="titinput" type="text" class="form-control input-sm" name="author" ng-model="commentData.author" placeholder="Titre">
				</div>
				<div class="form-group">
					<!--<input class="form-control input-lg" name="comment" ng-model="commentData.text" placeholder="Say what you have to say">-->
					<textarea class="form-control" id="textadesc" name="text" ng-model="commentData.text" cols="30" rows="5" placeholder="Description de l'annonce"></textarea>
				</div>
				<div class="form-group text-left subads">
					<button type="submit" class="btn btn-primary btn-lg">Submit</button>
				</div>
				<pre>
					<% commentData %>
				</pre>
			</div>
		</form>
	</div>
	<p class="text-center" ng-show="loading"><span class="fa fa-meh-o fa-5x fa-spin"></span></p>
	<div class="articleindex">
	   <div class="onebillet well" ng-hide="loading" ng-repeat="comment in comments">
	        <span class="titre"><% comment.titre %></span>
	        <p class="description">
	           <span class="prixlabel label label-info">Prix : <% comment.prix %>€</span> <br><br>
	           <img class="imgadd" src="img/avatar.png" alt="" />
	           <span onclick="return confirm('Voulez-vous vraiment supprimer cette annonce ?');" ng-click="deleteComment(comment.id)" class="btn btn-default trashad fa fa-trash"> </span>
	        </p>
	        <span ng-click="editComment(comment.id)" class="btn btn-default fa fa-pencil editad" data-toggle="modal" data-target-id="<% comment.id %>" data-target="#myModal">
			</span>

			<div class="modal fade formedit" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			  <div class="modal-dialog">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
			      </div>
			      <div class="modal-body">
					<div>
				        <form data-id="<% comment.id %>" id="formupdate" ng-submit="updateComment(comment.id)">
							<div class="form-group">
								<input type="text" class="form-control input-sm" name="author" ng-model="updateData.author" placeholder="Titre">
							</div>
							<div class="form-group">
								<!--<input class="form-control input-lg" name="comment" ng-model="commentData.text" placeholder="Say what you have to say">-->
								<textarea class="form-control" id="textadesc" name="text" ng-model="updateData.text" value="data.text" cols="30" rows="5" placeholder="Description de l'annonce"></textarea>
							</div>
							<div class="form-group text-left subads">
								<button type="submit" class="btn btn-primary btn-lg">Submit</button>
							</div>
						</form>
					</div>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			        <button type="button" class="btn btn-primary">Save changes</button>
			      </div>
			    </div>
			  </div>
			</div>
			<script>
				$('.formedit').hide();
				$('.editad').click(function(){
					$('.formedit').hide();
					var target_id = $(this).attr('data-target-id');
					$('.formedit[data-id = ' + target_id + ']').show();
				});
			</script>
	        @if(Auth::check())
	            <a href="#" class="accesview btn btn-primary btn-sm"><i class="fa fa-star"></i></a><br>
	            <span class="viewadd">Publié le <% comment.created_at %> &nbsp;</span>
	            <div class="buttonadd">

	                    <a class="btn btn-default fa fa-pencil" href="{{ URL::to('annonce/') }}"></a>
	                    <a class="deladd btn btn-danger fa fa-trash" href="{{ URL::to('delete/annonce/') }}" onclick="return confirm('Voulez vous vraiment supprimer cette annonce ?');"></a>
	            </div>
	        @endif
	    </div>
	</div>
</div>
@stop