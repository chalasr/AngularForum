@extends('layouts.master')

@section('content')
<div class="col-md-8 col-md-offset-2">

<!-- Formulaire de création d'annonces -->
	<div class="row well">
		<form id="formpost" ng-submit="submitThread()">
			<div class="createform">
				<div class="form-group">
					<select class="form-control input-sm titinput" name="subcategory" ng-model="commentData.subcategory" placeholder="Catégorie">
					@foreach($subcats as $subcat)
						<option value="{{ $subcat->id }}">{{ $subcat->name }}</option>
					@endforeach
					</select>
				</div>
				<div class="form-group">
					<input class="titinput" type="text" class="form-control input-sm" name="author" ng-model="commentData.author" placeholder="Titre">
				</div>
				<div class="form-group">
					<textarea class="form-control" id="textadesc" name="text" ng-model="commentData.text" cols="30" rows="5" placeholder="Description de l'annonce"></textarea>
				</div>
				<div class="form-group text-left subads">
					<button type="submit" class="subbtn btn btn-primary ">Submit</button>
				</div>
				<pre>
					<% commentData %>
				</pre>
			</div>
		</form>
	</div>
<!-- </Création> -->

    <div class="alert alert-success"></div>
    <div class="alert alert-danger"></div>
	<div class="articleindex homeb">
<!-- Loading image -->
	<p class="text-center" ng-show="loading"><img src="css/images/logospin.png" class="fa-spin"/></p>
<!-- </Loading image -->

<!-- CATEGORIES-->
	    <div class="onebillet well categorylist" ng-hide="loading" ng-repeat="comment in comments" ng-click="getSubCategories(comment.id)">
	   		<div class="category">
	        	<span class="titre"><% comment.name %></span>
	            <p class="describe_category"><% comment.description %></p>
	        </div>
    	</div>
<!-- FIN CATEGORIES -->

<!-- SUB_CATEGORIES -->
	    <div class="subcats">
	    	<input type="hidden" data-cat-id="<% comment.id %>" name="cat_id">
	    	<ul>
	    		<hr>
				<li class="subcatname">
					<p id="subcatnam" ng-repeat="subcat in subcats" ng-click="getThreads(subcat.id)" class="btn btn-primary inverse btn-lg souscategorie" data-sub-id="<% subcat.category_id %>"><% subcat.name %></p>
					<hr>
				</li>
			</ul>
	    </div>
<!-- FIN SUB_CATEGORIES -->

<!-- LIST DES THREADS PAR SUBCATEGORY -->
	<div class="threadsowner">
	    <div class="threadlist onebillet well" ng-hide="loading" ng-repeat="thread in threads" ng-click="showThread(thread.id)">
   	    	<span>
		   		<div class="category">
		   		<span data-target-id="<% thread.id %>" id="subcat_id" style="display:none;"><% thread.subcategory_id %></span>
		        	<span class="titre"><% thread.name %></span>
		        	<p class="sub_category">
			        	<span class="label label-primary">
			        	<i class="tagicon fa fa-tags"> </i>&nbsp;<% thread.subcategory_name %> &nbsp;
			        	</span>
		        	</p>
		        </div>
	    	</span>
    	</div>
    </div>
<!-- FIN DES THREADS -->
<!-- PREVIEW THREAD -->
    	<div data-id="<% thread.id %>" class="onethread well">
            <div class="article-header">
                <h1><% onethreads.name %></h1>
                <p class="sub_category2"><span class="label label-primary"><i class="tagicon fa fa-tags"> </i> &nbsp; <% onethreads.subcategory_name %> &nbsp; </span></p>
                <hr>
            </div>
            <div class="contenu">
                 <% onethreads.description %>
            </div>
            <div class="row photosedit">
            </div>
            <a href="{{ URL::to('<% onethreads.id %>') }}" class="viewbtn btn btn-primary inverse">Accéder au sujet</a>
            <!--<a href="#" id="contactv" class="iconprof btn btn-default fa fa-phone"> Contacter le vendeur</a>-->
            <p class="tags">Publié le :&nbsp; <% onethreads.created_at %></p>
		</div>
<!-- FIN PREVIEW THREAD -->
	</div>
	<div class="row well bestans" style="float:right;">
		<h4 class="titlebest">10 MEILLEURES RÉPONSES</h4>
		<div class="multans" ng-repeat="best in bests" >
			<a href="<% best.thread_id %>">
				<div class="ansblock well">
						<span class="bestauth"><% best.author_name %></span>
						<span class="bestdesc"><% best.created_at %></span>
						<span class="label label-info voteb"><% best.votes %></span>
				</div>
			</a>
		</div>
	</div>
</div>
	<script>
		$('.bestans').click(function(){
			var bestid = $('.bestid').val();
			$('ansblock').attr( 'onclick', 'location='+ bestid);
		});
		var target_id = $(this).attr('data-target-id');
		var subcat_id = $('#subcat_id').text();

		// $('#newpost').click(function(){
		// 	$('#subcategory_id').val(subcat_id);
		// });
		// $('.onethread').hide();
		// $('.category').click(function(){
		// 	var target_id = $(this).attr('data-target-id');
		// 	$('.threadlist').css('height', '600px !important');
		// 	$('.onethread[data-id = ' + target_id + ']').show();
		// });
		// $('#subcatnam').click(function(){
		// 	$('.categorylist').hide();
		// });
	</script>
@stop
