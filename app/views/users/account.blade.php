@extends('layouts.master')

@section('content')
<div class="col-md-8 col-md-offset-2">
<div class="row well">
        <form id="formpost" ng-submit="submitThread()">
            <div class="createform">
                <div class="form-group">
                    <input class="titinput" type="text" class="form-control input-sm" name="author" ng-model="threadData.author" placeholder="Titre">
                </div>
                <div class="form-group">
                    <!--<input class="form-control input-lg" name="thread" ng-model="threadData.text" placeholder="Say what you have to say">-->
                    <textarea class="form-control" id="textadesc" name="text" ng-model="threadData.text" cols="30" rows="5" placeholder="Description de l'annonce"></textarea>
                </div>
                <div class="form-group text-left subads">
                    <button type="submit" class="subbtn btn btn-primary btn-lg">Submit</button>
                </div>
                <pre>
                    <% threadData %>
                </pre>
            </div>
        </form>
    </div>
<div class="well articleindex">
    <div class="row">
        <div class="well article-complet">
      <a href="{{ URL::to('edit/'.Auth::user()->id) }}" style="float:right;" class="iconprof btn btn-primary inverse fa fa-wrench"></a>
    <a href="{{ URL::to('delete/'.Auth::user()->id) }}" class="iconprof2 btn btn-primary inverse fa fa-trash" onclick="return confirm('Voulez-vous vraiment supprimer votre compte ?');"></a>

            @foreach($user as $v)
                <div class="article-header">
                    <h1>{{ $v->username }}</h1>
                    <p>{{ ucfirst($v->role->name) }} depuis le {{ Thread::show($v->created_at) }}</p>
                    <hr>
                </div>
                <div class="contenu">
                    {{ $v->email }}
                </div>
                <p class="tags">tags :</p>
            @endforeach

     <div class="panel panel-default">
        <table class="table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Titre</th>
                    <th>Date de création</th>
                    <th>Actions</th>
                </tr>
            </thead>

            <tbody>
                <tr class="tr" ng-repeat="thread in threads">
                    <td id="threaid"><% thread.id %></td>
                    <td><% thread.name %></td>
                    <td><% thread.created_at %></td>
                    <td id="tdid">
                        <span class="fa fa-pencil threadbtn"></span> &nbsp;
                        <span class="fa fa-remove threadbtn" onclick="return confirm('Voulez vous vraiment supprimer cette annonce ?');"></span>
                    </td>
                </tr>
            </tbody>
        </table>
        <hr>
        </div>
    </div>
    </div>
</div>
</div>
<script>
// var threadid = $('#threadid').text();
// $( "#tdid" ).html(function() {
// return '<span class="fa fa-eye threadbtn showthread" onclick="location.href=\' ' + threadid + '\'"></span>&nbsp';
// });
</script>
@stop