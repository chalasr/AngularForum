@extends('layouts.master')

@section('content')
<div class="row">
    <div class="row well">
        <form id="formanswer" ng-submit="submitAnswer()">
            <div class="createform">
                <div class="form-group">
                    <textarea class="form-control" id="textadesc" name="text" ng-model="commentData.text" cols="30" rows="5" placeholder="Votre réponse"></textarea>
                </div>
                <input type="hidden" id="subcategory" name="subcategory" ng-model="commentData.subcategory" value="">
                <div class="form-group text-left subads">
                    <button type="submit" class="subbtn btn btn-primary ">Submit</button>
                </div>
                <pre>
                    <% commentData %>
                </pre>
            </div>
        </form>
    </div>
    <div class="alert alert-success"></div>
    <div class="alert alert-danger"></div>
    <div class="well articleindex viewthread">
        @foreach($annonce as $v)
            <div class="article-header">
                <h1>{{ $v->name }} </h1>
                <span class="sumvotes label label-info">
                <% votes %>
                </span>
                <p class="sub_category3">
                    <span class="label label-primary"><i class="tagicon fa fa-tags"> </i> &nbsp;{{ $v->subcategory_name }} &nbsp;</span>
                    <span style="display:none;" id="subcat_id">{{ $v->subcategory_id }}</span>
                </p>
                @if(Auth::check())
                    <span class="votebtn">
                    <input id="nbrvote" type="hidden" value="<% sumvotes %>" name="nbrvote">
                            <div>
                                <form id="addvote" ng-submit="addVote()">
                                    <input type="hidden" name="threadid" value="{{ $v->id }}" ng-model="voteData.threadid">
                                    <input type="hidden" name="vote" value="1" ng-model="voteData.vote">
                                    <button type="submit" id="upbtn" class="upbtn btn btn-primary inverse btn-sm fa fa-thumbs-o-up"></button>
                                </form>
                            </div>
                            <div>
                                <form id="delvote" ng-submit="removeVote()">
                                    <input type="hidden" name="threadid" value="{{ $v->id }}" ng-model="badvoteData.threadid">
                                    <input type="hidden" name="vote" value="-1" ng-model="badvoteData.vote">
                                    <button type="submit" id="downbtn" class="btn btn-primary inverse btn-sm fa fa-thumbs-o-down downbtn"></button>
                                </form>
                            </div>
                    </span>
                @endif
                <hr>
            </div>
            <div class="contenu">
                {{ $v->description }}
            </div>
            <div class="row photosedit"></div>
            <!--<a href="#" id="contactv" class="iconprof btn btn-default fa fa-phone"> Contacter le vendeur</a>-->
            <p class="tags">Posté le {{ Thread::show($v->created_at) }} </p>

                <div class="crud">
                @if(Auth::check())
                    @if(Auth::user()->role_id == 3 || $v->author_id == Auth::user()->id)
                    <form ng-submit="addFollow()">
                        <input id="nbrfol" type="hidden" value="<% follows %>" name="nbrfol">
                        <input type="hidden" name="threadid" value="{{ $v->id }}" ng-model="followData.threadid">
                        <button id="followth" class="btn btn-default btn-sm fa fa-star vanswer"></button>
                    </form>
                    <span ng-click="deleteThread({{ $v->id }})" class="btn btn-danger btn-sm trashad2 fa fa-trash"></span>
                    <span ng-click="editThread({{ $v->id}})" class="btn btn-info btn-sm fa fa-pencil editad2" data-toggle="modal" data-target-id="<% comment.id %>" data-target="#myModal"></span>
                    @endif
                @endif
                <span id="newanswer" class="btn btn-primary inverse repondre"> <span>Répondre&nbsp;</span><i class="iconup fa fa-reply"></i></span>
            </div>
            <div ng-repeat="answer in answers">
                <hr>
                @if(Auth::check())
                <div style="display:flex;">
                    <span>
                        <input type="hidden" id="nbrans" value="<% sumansvote %>" name="ansvote">
                        <form id="addvoteans" ng-submit="addVoteAns(answer.id)">
                            <input type="hidden" name="threadid" value="{{ $v->id }}" ng-model="ansData.threadid">
                            <input type="hidden" name="voteans" value="1" ng-model="ansData.voteans">
                            <button type="submit" class="ansup btn btn-primary inverse btn-sm fa fa-thumbs-o-up"></button>
                        </form>
                        <form id="delvoteans" ng-submit="delVoteAns(answer.id)">
                            <input type="hidden" name="threadid" value="{{ $v->id }}" ng-model="badansData.threadid">
                            <input type="hidden" name="answerid" value="<% answer.id %>" ng-model="badansData.answerid">
                            <input type="hidden" name="voteans" value="-1" ng-model="badansData.voteans">
                            <button type="submit" class="ansbad btn btn-primary inverse btn-sm fa fa-thumbs-o-down"></button>
                        </form>
                    <span class="ansumv label label-info">
                        <% answer.votes %>
                    </span>
                    </span>
                @endif
                     <span class="nameans"><% answer.author_name %></span>
                     <span class="descans"><% answer.description %></span>
                </div>
            </div>
            <div class="modal fade formedit" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                  </div>
                  <div class="modal-body">
                    <div>
                        <form data-id="<% comment.id %>" id="formupdate" ng-submit="updateThread(onethreads.id)">
                            <div class="form-group">
                                <input type="text" class="titinput" class="titleup form-control input-sm" name="author" ng-model="updateData.author" value="<% updateData.name %>" placeholder="<% updateData.name %>">
                            </div>
                            <div class="form-group">
                                <!--<input class="form-control input-lg" name="comment" ng-model="commentData.text" placeholder="Say what you have to say">-->
                                <textarea class="form-control" id="textadesc" name="text" ng-model="updateData.text" cols="30" rows="5" value="<% updateData.description %>" placeholder="<% updateData.description %>"></textarea>
                            </div>
                            <div class="form-group text-left subads">
                                <button type="submit" class="subbtn btn btn-primary">Sauvegarder</button>
                            </div>
                        </form>
                        <pre>
                            <% updateData %>
                        </pre>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                  </div>
                </div>
              </div>
            </div>
        </div>
        @endforeach
</div>
<script>
$(function(){
    var subcat_id = $('#subcat_id').text();
    $('#newanswer').click(function(){
        $('#subcategory').val(subcat_id);
    });
});

$(document).ready(function(){

    setTimeout(function() {
        var sumvote = $('#nbrvote').val();
        if(sumvote > 0){
            $('.upbtn').addClass('disabled');
            $('.downbtn').removeClass('disabled');
        }else if(sumvote == 0){
            $('.downbtn').removeClass('disabled');
            $('.upbtn').removeClass('disabled');
        }else if(sumvote < 0){
            $('.downbtn').addClass('disabled');
            $('.upbtn').removeClass('disabled');
        }
    }, 200);

    $('.upbtn').click(function() {
        setTimeout(function() {
            var sumvote = $('#nbrvote').val();
            if(sumvote > 0){
                $('.upbtn').addClass('disabled');
                $('.downbtn').removeClass('disabled');
            }else if(sumvote == 0){
                $('.downbtn').removeClass('disabled');
                $('.upbtn').removeClass('disabled');
            }else if(sumvote < 0){
                $('.downbtn').addClass('disabled');
                $('.upbtn').removeClass('disabled');
            }
        }, 200);
    });

    $('.downbtn').click(function() {
         setTimeout(function() {
            var sumvote = $('#nbrvote').val();
            if(sumvote > 0){
                $('.upbtn').addClass('disabled');
                $('.downbtn').removeClass('disabled');
            }else if(sumvote == 0){
                $('.downbtn').removeClass('disabled');
                $('.upbtn').removeClass('disabled');
            }else if(sumvote < 0){
                $('.downbtn').addClass('disabled');
                $('.upbtn').removeClass('disabled');
            }
        }, 200);
    });

    $(function() {
         setTimeout(function() {
            var havefol = $('#nbrfol').val();
            if(havefol > 0){
                $('.vanswer').addClass('disabled');
                $('.vanswer')
            }else if(havefol == 0){
                $('.vanswer').removeClass('disabled');
            }
        }, 200);
    });

    $('.vanswer').click(function() {
         setTimeout(function() {
            var havefol = $('#nbrfol').val();
            if(havefol > 0){
                $('.vanswer').addClass('disabled');
            }else if(havefol == 0){
                $('.vanswer').removeClass('disabled');
            }
        }, 200);
    });

});

</script>
@stop
