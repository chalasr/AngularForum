angular.module('commentService', [])

	.factory('Category', function($http) {

		return {
			get : function() {
				return $http.get('api/categories');
			},
			getCategories : function() {
				return $http.get('../api/categories');
			},
			getsubcat : function(){
				return $http.get('../Big_project/forum/public/api/subcategories/');
			},
			getsub : function(id){
				return $http.get('api/getsubcategories/' + id);
			},
			getAnswers : function(id){
				return $http.get('api/getanswers/' + id);
			},
			getVotes : function(id){
				return $http.get('api/getvotes/' + id)
			},
			getVoteAns : function(id){
				return $http.get('api/getvoteans/' + id)
			},
			getFollows : function(id){
				return $http.get('api/getfollows/' + id)
			},
			bestAnswers : function(){
				return $http.get('http://localhost/Big_project/forum/public/api/bestanswers')
			},
			sumVotes : function(id){
				return $http.get('api/sumvotes/' + id)
			},
			sumVoteAns : function(id){
				return $http.get('api/sumvoteans/' + id)
			},
			allSumAns : function(id){
				return $http.get('api/sumallans/' + id + '/' + answerid)
			},
			show : function(id) {
				return $http.get('api/threads/' + id);
			},
			myThreads : function() {
				return $http.get('api/mythreads');
			},
			showThreads : function(id) {
				return $http.get('api/showthreads/' + id);
			},
			save : function(commentData) {
				return $http({
					method: 'POST',
					url: 'api/threads',
					headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
					data: $.param(commentData)
				});
			},
			saveAnswer : function(commentData) {
				var lol = window.location.href.split("/");
				value = $.param(commentData)+"&threadid="+lol[6];
				return $http({
					method: 'POST',
					url: 'api/answers',
					headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
					data: value
				});
			},
			saveCategory : function(commentData) {
				return $http({
					method: 'POST',
					url: '../api/categories',
					headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
					data: $.param(commentData)
				});
			},
			saveSubCat : function(subData) {
				return $http({
					method: 'POST',
					url: '../api/subcategories',
					headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
					data: $.param(subData)
				});
			},
			addVote : function(voteData) {
				var lol = window.location.href.split("/");
				value = $.param(voteData)+"&threadid="+lol[6]+"&vote=1";
				return $http({
					method: 'POST',
					url: 'api/votes',
					headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
					data: value
				});
			},
			removeVote : function(badvoteData) {
				var lol = window.location.href.split("/");
				value = $.param(badvoteData)+"&threadid="+lol[6]+"&vote=-1";
				return $http({
					method: 'POST',
					url: 'api/votes',
					headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
					data: value
				});
			},
			addVoteAns : function(id, ansData) {
				var lol = window.location.href.split("/");
				value = $.param(ansData)+"&threadid="+lol[6]+"&voteans=1"+"&answerid="+id;
				return $http({
					method: 'POST',
					url: 'api/ansvotes',
					headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
					data: value
				});
			},
			removeVoteAns : function(id, badansData) {
				var lol = window.location.href.split("/");
				value = $.param(badansData)+"&threadid="+lol[6]+"&voteans=-1"+"&answerid="+id;
				return $http({
					method: 'POST',
					url: 'api/ansvotes',
					headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
					data: value
				});
			},
			follow : function(followData) {
				var lol = window.location.href.split("/");
				value = $.param(followData)+"&threadid="+lol[6];
				return $http({
					method: 'POST',
					url: 'api/follows',
					headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
					data: value
				});
			},
			update : function(id, updateData) {
				return $http.put('api/threads/' + id, updateData);
			},
			destroy : function(id) {
				return $http.delete('http://localhost/Big_project/forum/public/api/threads/' + id);
			},
			destroySub : function(id) {
				return $http.delete('../api/subcategories/' + id);
			},
			destroyCat : function(id) {
				return $http.delete('../api/categories/' + id);
			},
			destroyAnswer : function(id) {
				return $http.delete('http://localhost/Big_project/forum/public/api/answers/' + id);
			},
			//ADMIN
			getUsers : function() {
				return $http.get('api/admin/membres');
			}
		}

	});