angular.module('myApp.services').factory('Comment', function($resource) {
  return $resource('/api/comments/:id', { id: '@_id' }, {
    update: {
      method: 'PUT' // this method issues a PUT request
    }
  }, {
    stripTrailingSlashes: false
  });
});