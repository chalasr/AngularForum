angular.module('mainCtrl', [])

	.controller('mainController', function($scope, $http, Category) {

		// Declare mes variables
		$scope.commentData = {};
		$scope.updateData = {};
		$scope.answerData = {};
		$scope.voteData = {};
		$scope.followData = {};
		$scope.badvoteData = {};
		$scope.subData = {};
		$scope.ansData = {};
		$scope.badansData = {};
		$scope.loading = true;

		var uri = window.location.href.split("/");
		var value = uri[6]; // @param id
	    var threadid = /\d/g; //RegExp. all numbers digits (0 a 9 et +)

		// Récupère les différentes catégories et les affiche
		if(window.location.pathname == '/Big_project/forum/public/'){
			console.log(window.location.pathname);
			Category.get()
				.success(function(data){
					$('.categorylist').fadeIn();
					$scope.comments = data;
					$scope.loading = false;
				});
			Category.bestAnswers()
				.success(function(data){
					$scope.bests = data;
				});
		// Affiche le compte de l'utilisateur courant
		}else if(window.location.pathname == '/Big_project/forum/public/account'){
			Category.myThreads()
				.success(function(getData){
					$scope.threads = getData;
					$scope.loading = false;
				});
		}else if(window.location.pathname == '/Big_project/forum/public/admin/forum'){
			Category.getCategories()
				.success(function(getData){
					$scope.categories = getData;
					$scope.loading = false;
				});
			Category.getsubcat()
				.success(function(data){
					$scope.subcats = data;
					$scope.loading = false;
				});
		}else if(value.match(threadid)){
			console.log('answeeers');
			Category.getAnswers(value)
				.success(function(getData){
					$scope.answers = getData;
				});
			Category.getVotes(value)
				.success(function(data){
					$scope.votes = data;
				});
			Category.sumVotes(value)
				.success(function(data){
					$scope.sumvotes = data;
				});
			Category.getFollows(value)
				.success(function(data){
					$scope.follows = data;
				});
		};


/*
* THREADS
*/

		/**
		* Mes Threads
		* @return data
		**/
		$scope.showMyThreads = function() {
			$scope.loading = true;
			$('.threadlist').hide();
			$('.subcats').hide();
			$('.onethread').hide();
			Category.myThreads()
				.success(function(getData) {
					$scope.comments = getData;
					$scope.loading = false;
				})
				.error(function(data) {
					console.log(data);
				});
		};

		/**
		* Affiche les threads par sous_catégorie
		* @return data
		**/
		$scope.getThreads = function(id){
		$('.categorylist').hide();
			Category.showThreads(id)
				.success(function(getData){
					$scope.threads = getData;
					$('.threadsowner').fadeIn();
				})
				.error(function(data){
					console.log(data);
				});
		};

		/**
		* Création d'un thread
		* @return data
		**/
		$scope.submitThread = function() {
			$scope.loading = true;
			Category.save($scope.commentData)
				.success(function(data) {
					console.log('reussite');
					$('#formpost').slideToggle();
					$('.alert-success').text('Votre annonce est en ligne !');
					$('.alert-success').slideToggle();
					setTimeout("$('.alert-success').slideToggle(500);",4000 );
					// if successful, we'll need to refresh the comment list
					Category.get()
					 	.success(function(getData) {
					 		$('.threadlist').hide();
					 		$('.subcats').hide();
					 		$('.categorylist').show();
					 		$scope.comments = getData;
					 		$scope.loading = false;
						});
					})
					.error(function(data) {
						console.log(data);
					});
		};


		/**
		* voir une d'annonce
		* @return old data
		**/
		$scope.showThread = function(id) {
			$('.onethread').hide();
			Category.show(id)
				.success(function(data){
					$scope.onethreads = data;
					$('.onethread').fadeIn();
					//$scope.loading = false;
				});
		};


		/**
		* Edition d'annonce
		* @return old data
		**/
		$scope.editThread = function(id) {
			Category.show(id)
				.success(function(data){
					$scope.updateData = data;
				});
		};

		/**
		* Update thread
		* @return new data
		**/
		$scope.updateThread = function(){
			var id = $scope.updateData.id;
			$scope.loading = true;
			Category.update(id, $scope.updateData).success(function(){
				javascript:window.location.reload();
				$('#myModal').modal('hide');
				$('.modal-backdrop').hide();
				$('.onethread').hide();
				$('.alert-success').text('Votre sujet a bien été modifié');
				$('.alert-success').slideToggle();
				setTimeout("$('.alert-success').slideToggle(500);",4000 );
				Category.get()
					.success(function(getData) {
						$scope.comments = getData;
						$scope.loading = false;
					});
			})
			.error(function(data) {
				console.log(data);
			});
		};

		/**
		* Delete thread
		* @return old data
		**/
		$scope.deleteThread = function(id) {
			Category.destroy(id)
				.success(function(data) {
					$('.alert-success').text('Votre thread a bien été supprimé !');
					$('.alert-success').slideToggle();
					setTimeout("$('.alert-success').slideToggle(500);",4000 );
					if(window.location.pathname != '/Big_project/forum/public/admin')
					document.location.href="http://localhost/Big_project/forum/public/";
					else
					javascript:window.location.reload();
					// if successful, we'll need to refresh the comment list
					Category.get()
						.success(function(getData) {
							$scope.comments = getData;
						});

				});
		};

		/**
		* Affiche les sous catégories
		* @return data
		**/
		$scope.getSubCategories = function(id){
		//$scope.loading = true;
			Category.getsub(id)
				.success(function(getData){
					$scope.subcats = getData;
					//$scope.loading = false;
					$('.subcats').fadeIn();
				})
				.error(function(data){
					console.log(data);
				});
		};

/**
* VOTES
**/
		/**
		* voir une d'annonce
		* @return old data
		**/
		$scope.getVotes = function(id) {
			Category.getVotes(value)
				.success(function(data){
					$scope.votes = data;
				});
		};

		/**
		* voir une d'annonce
		* @return old data
		**/
		$scope.getAnsVotes = function(id) {
			Category.getVoteAns(id)
				.success(function(data){
					$scope.ansum = data;
				});
		};

		/**
		* Add Vote
		**/
		$scope.addVote = function() {
			Category.addVote($scope.voteData)
				.success(function(data){
					Category.getVotes(value)
						.success(function(data){
							$scope.votes = data;
						});
					Category.sumVotes(value)
						.success(function(data){
							$scope.sumvotes = data;
						});
				})
				.error(function(data) {
					console.log(data);
				});
		};

		/**
		* Remove Vote
		**/
		$scope.removeVote = function() {
			Category.removeVote($scope.badvoteData)
				.success(function(data){
					Category.getVotes(value)
						.success(function(data){
							$scope.votes = data;
						});
					Category.sumVotes(value)
						.success(function(data){
							$scope.sumvotes = data;
						});
				})
				.error(function(data) {
					console.log(data);
				});
		};

		/**
		* Add Vote
		**/
		$scope.addVoteAns = function(id) {
			Category.addVoteAns(id, $scope.ansData)
				.success(function(data){
					Category.getAnswers(value)
						.success(function(getData){
							$scope.answers = getData;
						});
					Category.sumVoteAns(id)
						.success(function(data){
							$scope.sumansvote = data;
						});
				})
				.error(function(data) {
					console.log(data);
				});
		};

		/**
		* Remove Vote
		**/
		$scope.delVoteAns = function(id) {
			Category.removeVoteAns(id, $scope.badansData)
				.success(function(data){
					Category.getAnswers(value)
						.success(function(getData){
							$scope.answers = getData;
						});
					Category.sumVoteAns(id)
						.success(function(data){
							$scope.sumansvote = data;
						});
				})
				.error(function(data) {
					console.log(data);
				});
		};

/**
*FOLLOWS
**/
		/**
		* récupère les followers
		* @return old data
		**/
		$scope.getFollows = function(id) {
			Category.getFollows(id)
				.success(function(data){
					$scope.follows = data;
				});
		};

		/**
		* Follow un thread
		**/
		$scope.addFollow = function() {
			var uri = window.location.href.split("/");
			var value = uri[6];
			Category.follow($scope.followData)
				.success(function(data){
					Category.getFollows(value)
						.success(function(data){
							$scope.follows = data;
						});
				})
				.error(function(data) {
					console.log(data);
				});
		};

/**
* ANSWERS
**/
		/**
		* Edition d'annonce
		* @return old data
		**/
		$scope.getAnswers = function(id) {
			Category.getAnswers(id)
				.success(function(data){
					$scope.answers = data;
				});
		};

		/**
		* Création d'une réponse
		* @return data
		**/
		$scope.submitAnswer = function() {
			var uri = window.location.href.split("/");
			var value = uri[6];
			var sub_id = $('#subcategory').val();
			$scope.commentData.subcategory = sub_id;
			Category.saveAnswer($scope.commentData)
				.success(function(data) {
					console.log(data);
					$('#formanswer').slideToggle();
					$('.alert-success').text('Votre réponse est en ligne !');
					$('.alert-success').slideToggle();
					setTimeout("$('.alert-success').slideToggle(500);",4000 );
					Category.getAnswers(value)
					 	.success(function(getData) {
					 		$scope.answers = getData;
						});
				})
				.error(function(data) {
					console.log(data);
					$('#formanswer').slideToggle();
					$('.alert-danger').text('Un problème est survenu !');
					$('.alert-danger').slideToggle();
					setTimeout("$('.alert-danger').slideToggle(500);",4000 );
				});
		};


/**
* ADMIN
**/

	/**
	* CATEGORIES
	**/

		/**
		* Création d'une categorie
		* @return data
		**/
		$scope.submitCategory = function() {
			javascript:window.location.reload();
			Category.saveCategory($scope.commentData)
				.success(function(data) {
					console.log('reussite');
					$('#formpost').slideToggle();
					$('.alert-success').text('Votre catégorie a été créée !');
					$('.alert-success').slideToggle();
					setTimeout("$('.alert-success').slideToggle(500);",4000 );
					// if successful, we'll need to refresh the comment list
					Category.getCategories()
					 	.success(function(getData) {
					 		$scope.categories = getData;
						});
					})
					.error(function(data) {
						console.log(data);
					});
		};

		/**
		* Delete thread
		* @return old data
		**/
		$scope.deleteCat = function(id) {
			Category.destroyCat(id)
				.success(function(data) {
					$('.alert-success').text('La catégorie a bien été supprimé !');
					$('.alert-success').slideToggle();
					setTimeout("$('.alert-success').slideToggle(500);",4000 );
					Category.getCategories()
						.success(function(getData) {
							$scope.categories = getData;
						});

				});
		};

	/**
	* SUBCATEGORIES
	**/

		/**
		* Création d'une categorie
		* @return data
		**/
		$scope.submitSubCat = function() {
			Category.saveSubCat($scope.subData)
				.success(function(data) {
					console.log('reussite');
					$('#formsub').slideToggle();
					$('.alert-success').text('Votre sous-catégorie a été créée !');
					$('.alert-success').slideToggle();
					setTimeout("$('.alert-success').slideToggle(500);",4000 );
					// if successful, we'll need to refresh the comment list
					Category.getsubcat()
					 	.success(function(data) {
					 		$scope.subcats = data;
						});
					})
					.error(function(data) {
						console.log(data);
					});
		};


		/**
		* Delete subcategory
		* @return old data
		**/
		$scope.deleteSubcat = function(id) {
			Category.destroySub(id)
				.success(function(data) {
					$('.alert-success').text('La sous-catégorie a bien été supprimé !');
					$('.alert-success').slideToggle();
					setTimeout("$('.alert-success').slideToggle(500);",4000 );
					Category.getsubcat()
						.success(function(getData) {
							$scope.subcats = getData;
						});

				});
		};

	/*
	* ANSWERS
	*/
		/**
		* Delete answer
		* @return old data
		**/
		$scope.deleteAnswer = function(id) {
			Category.destroyAnswer(id)
				.success(function(data) {
				javascript:window.location.reload();
					$('.alert-success').text('La réponse a bien été supprimé !');
					$('.alert-success').slideToggle();
					setTimeout("$('.alert-success').slideToggle(500);",4000 );
				});
		};

	});
